Source: maliit-framework
Section: libs
Priority: extra
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Norbert Preining <norbert@preining.info>,
Build-Depends: cmake,
               debhelper-compat (= 13),
               doxygen,
               libdbus-1-dev,
               libglib2.0-dev,
               libkf5wayland-dev,
               libmtdev-dev,
               libudev-dev,
               libwayland-dev,
               libxcb-composite0-dev,
               libxcb-damage0-dev,
               libxcb-xfixes0-dev,
               libxext-dev,
               pkg-config,
               qtbase5-dev (>= 5.2),
               qtbase5-private-dev,
               qtdeclarative5-dev,
               qtwayland5-dev-tools,
               qtwayland5-private-dev,
               wayland-protocols
Standards-Version: 4.5.1
Homepage: https://github.com/maliit/framework
Vcs-Browser: https://salsa.debian.org/qt-kde-team/extras/maliit-framework
Vcs-Git: https://salsa.debian.org/qt-kde-team/extras/maliit-framework.git
Rules-Requires-Root: no

Package: maliit-framework
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, qml-module-qtquick-localstorage
Recommends: maliit-inputmethod-plugin
Breaks: gir1.2-maliit-1.0 (<< ${source:Version}),
        libmaliit-glib0 (<< ${source:Version}),
        libmaliit0 (<< ${source:Version}),
        maliit-dbus-activation (<< ${source:Version}),
        maliit-inputcontext-qt4 (<< ${source:Version})
Description: Maliit Input Method Framework
 Maliit provides a flexible and cross-platform input method framework. It has a
 plugin-based client-server architecture where applications act as clients and
 communicate with the Maliit server via input context plugins. The communication
 link currently uses D-Bus. Maliit is an open source framework (LGPL 2) with
 open source plugins (BSD).

Package: libmaliit-plugins2
Architecture: any
Breaks: maliit-framework (<< 0.99), libmaliit-plugins0 (<< ${source:Version}~ciBuild)
Replaces: maliit-framework (<< 0.99), libmaliit-plugins0 (<< ${source:Version}~ciBuild)
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Maliit Input Method Framework - Plugin Library
 Maliit provides a flexible and cross-platform input method framework. It has a
 plugin-based client-server architecture where applications act as clients and
 communicate with the Maliit server via input context plugins. The communication
 link currently uses D-Bus. Maliit is an open source framework (LGPL 2) with
 open source plugins (BSD).
 .
 This package provides the maliit plugin library for the Maliit framework.

Package: maliit-framework-dev
Architecture: any
Section: libdevel
Depends: maliit-framework (= ${binary:Version}), ${misc:Depends}
Description: Maliit Input Method Framework - Development Files
 Maliit provides a flexible and cross-platform input method framework. It has a
 plugin-based client-server architecture where applications act as clients and
 communicate with the Maliit server via input context plugins. The communication
 link currently uses D-Bus. Maliit is an open source framework (LGPL 2) with
 open source plugins (BSD).
 .
 This package is needed to compile plugins for the Maliit framework

Package: maliit-framework-dbg
Section: debug
Architecture: any
Depends: maliit-framework (= ${binary:Version}), ${misc:Depends}
Description: Maliit Input Method Framework - Debug symbols
 Maliit provides a flexible and cross-platform input method framework. It has a
 plugin-based client-server architecture where applications act as clients and
 communicate with the Maliit server via input context plugins. The communication
 link currently uses D-Bus. Maliit is an open source framework (LGPL 2) with
 open source plugins (BSD).
 .
 This package contains debugging symbols.

Package: libmaliit-glib2
Depends: ${misc:Depends}, ${shlibs:Depends}
Architecture: any
Section: libs
Breaks: libmaliit-glib0 (<< ${source:Version}~ciBuild)
Replaces: libmaliit-glib0 (<< ${source:Version}~ciBuild)
Description: Maliit Input Method Framework - GLib Bindings
 Maliit provides a flexible and cross-platform input method framework. It has a
 plugin-based client-server architecture where applications act as clients and
 communicate with the Maliit server via input context plugins. The communication
 link currently uses D-Bus. Maliit is an open source framework (LGPL 2) with
 open source plugins (BSD).
 .
 This package provides GLib bindings for the Maliit framework.

Package: libmaliit-glib-dev
Depends: libmaliit-glib2 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Architecture: any
Section: libdevel
Description: Maliit Input Method Framework - GLib Development Headers
 Maliit provides a flexible and cross-platform input method framework. It has a
 plugin-based client-server architecture where applications act as clients and
 communicate with the Maliit server via input context plugins. The communication
 link currently uses D-Bus. Maliit is an open source framework (LGPL 2) with
 open source plugins (BSD).
 .
 This package provides GLib development headers for the Maliit framework.

Package: maliit-framework-doc
Depends: maliit-framework, ${misc:Depends}
Architecture: all
Section: docs
Description: docs for Maliit framework
 Documentation for the framework of Maliit
